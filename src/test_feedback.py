from feedback import construct_feedback

def toString(desc: str, value: int, selected: bool):
    if selected:
        return desc + ' is ' + str(value)
    else:
        return ''


def test_create():
    feedbackDesc = 'positive points for X'

    myfeedback = construct_feedback(feedbackDesc, 3, toString)

    assert myfeedback.getDescription() == feedbackDesc


def test_getValue():
    feedbackDesc = 'positive points for X'

    myfeedback = construct_feedback(feedbackDesc, 3, toString)

    assert myfeedback.getValue() == 3


def test_toString():
    feedbackDesc = 'positive points for X'

    myfeedback = construct_feedback(feedbackDesc, 3, toString)

    assert myfeedback.toString() == toString(feedbackDesc, 3, True)
