from typing import NamedTuple
from collections.abc import Callable

class Component(NamedTuple):
    functionA: Callable[[int], None]


def constructComponent() -> Component:
    def functionA(a: int) -> None:
        print('Value from component: ' + str(a))

    return Component(functionA=functionA)


def decoratorA(decorated: Component) -> Component:
    def functionA(a: int) -> None:
        print('Value from decoratorA: ' + str(a))
        decorated.functionA(a)

    return Component(functionA=functionA)


def decoratorB(decorated: Component) -> Component:
    def functionA(a: int) -> None:
        print('Value from decoratorB: ' + str(a))
        decorated.functionA(a)

    return Component(functionA=functionA)


# A more functional approach
def myFunction(value: int) -> str:
    return 'Value passed to myFunction: ' + str(value)


def constructDecorator(decorated: Callable[[int], str]) -> Callable[[int], str]:
    def decoratorFunction(value: int) -> str:
        result = decorated(value)
        return 'Value passed to decoratorFunction: ' + result

    return decoratorFunction


# The python approach
def decoratorC(func)
    def func_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return 'Value passed to func_wrapper: ' + result

    return func_wrapper