from typing import NamedTuple
from collections.abc import Callable

class StrategyConsumer(NamedTuple):
    consumeStrategy: Callable[[int, int], str]


def constructStrategyConsumer(strategy: Callable[[int], str]) -> StrategyConsumer:
    def consumeStrategy(a: int, b: int) -> str:
        return strategy(a + b)

    return StrategyConsumer(consumeStrategy=consumeStrategy)


def strategyA(a: int) -> str:
    return str(a)


def strategyB(a: int) -> str:
    return 'Value: ' + str(a)


def strategyC(a: int) -> str:
    if a > 0:
        return str(a)
    else:
        return 'Negative: ' + str(0-a)
