from factory import constructFactory, constructProduct, Factory, Product

def test_createProduct():
    factory = constructFactory()
    productA = factory.createProduct()
    productB = factory.createProduct()

    assert productA.getId() == productB.getId() - 1
