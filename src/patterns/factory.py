from typing import NamedTuple
from collections.abc import Callable

class Product(NamedTuple):
    getId: Callable[[], int]

class Factory(NamedTuple):
    createProduct: Callable[[], Product]


def constructProduct(id: int) -> Product:
    def getId() -> int:
        return id

    return Product(getId=getId)


def constructFactory() -> Factory:
    def idGenerator():
        id=0
        while True:
            id += 1
            yield id
    
    gen = idGenerator()

    def createProduct() -> Product:
        nextId = next(gen)
        return constructProduct(nextId)

    return Factory(createProduct=createProduct)
