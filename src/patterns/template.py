from typing import NamedTuple
from collections.abc import Callable

class Template(NamedTuple):
    functionA: Callable[[], int]
    functionB: Callable[[int], str]


class Templated(NamedTuple):
    functionA: Callable[[int, int], str]


def constructTemplated(template: Template) -> Templated:
    def functionA(a: int, b: int) -> str:
        c = template.functionA()
        d = template.functionB(a + b + c)

        return d

    return Templated(functionA=functionA)


def templateA() -> Template:
    def functionA() -> int:
        return 2


    def functionB(a: int) -> str:
        return str(a)


    return Template(functionA=functionA,
                    functionB=functionB)


def templateB() -> Template:
    def functionA() -> int:
        return 42


    def functionB(a: int) -> str:
        return 'value: ' + str(a)


    return Template(functionA=functionA,
                    functionB=functionB)
