from typing import NamedTuple
from collections.abc import Callable

class Feedback(NamedTuple):
    getDescription: Callable[[], str]
    getValue: Callable[[], int]
    toString: Callable[[], str]


def toString(desc: str, value: int, selected: bool):
    if selected:
        return desc + ': ' + str(value)
    else:
        return 'No feedback given for ' + desc


def construct_feedback(description: str, value: int, toStringFunc: Callable[[str, int, bool], str] = toString) -> Feedback:
    def getDescription() -> str:
        return description

    def getValue() -> int:
        return value

    def toString() -> str:
        return toStringFunc(description, value, True)

    if value < 0:
        raise Exception('x should be greater than 0. The value of x was: {}'.format(x))        
    if value > 5:
        raise Exception('x should not exceed 5. The value of x was: {}'.format(x))        

    return Feedback(getDescription=getDescription,
                    getValue=getValue,
                    toString=toString)
