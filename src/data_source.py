from typing import NamedTuple
from collections.abc import Callable

from course import Course
from feedback import Feedback

class DataSource(NamedTuple):
    getCourseList: Callable[[], list[Course]]
    getFeedbackList: Callable[[], list[Feedback]]


def construct_data_source(courseList: list[Course], feedbackList: list[Feedback]) -> DataSource:
    def getCourseList() -> list[Course]:
        return courseList


    def getFeedbackList(name: str) -> list[Feedback]:
        return feedbackList


    return DataSource(getCourseList=getCourseList,
                    getFeedbackList=getFeedbackList)
