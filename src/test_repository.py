from repository import construct_course_repository
from course import construct_course, Course
from feedback import construct_feedback, Feedback
from data_source import DataSource

def test_post():
    courseName = 'my course'
    courseId = 3

    mycourse = construct_course(courseId, courseName)

    myrepository = create_repository()
    myrepository.post(mycourse)

    courses = myrepository.getAll()

    assert len(courses) == 2


def test_get():
    courseName = 'my course'
    courseId = 3

    mycourse = construct_course(courseId, courseName)

    myrepository = create_repository()
    myrepository.post(mycourse)

    courses = myrepository.getAll()
    assert len(courses) == 2

    mycourse = myrepository.get(courseId)
    assert mycourse.getName() == courseName


def test_delete():
    courseName = 'my course'
    courseId = 3

    mycourse = construct_course(courseId, courseName)

    myrepository = create_repository()
    myrepository.post(mycourse)

    myrepository.delete(mycourse)

    courses = myrepository.getAll()

    assert len(courses) == 1


def create_repository(courseList: list[Course] = None):
    def getCourseList() -> list[Course]:
        return courseList

    if courseList is None:
        courseList = [construct_course(11, 'course name')]

    return construct_course_repository(
        DataSource(getCourseList=getCourseList,
                    getFeedbackList=None))
