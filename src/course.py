from typing import NamedTuple
from collections.abc import Callable

class Course(NamedTuple):
    getId: Callable[[], int]
    getName: Callable[[], str]
    setName: Callable[[str], 'Course']
    toString: Callable[[], str]


def construct_course(id: int, name: str) -> Course:
    def getId() -> int:
        return id


    def getName() -> str:
        return name


    def setName(name: str) -> Course:
        return construct_course(getId(), name)


    def toString() -> str:
        return str(id) + getName()


    return Course(
        getId=getId,
        getName=getName,
        setName=setName,
        toString=toString)
