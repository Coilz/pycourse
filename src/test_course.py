from course import construct_course

def test_getName():
    courseName = 'my course'
    courseId = 3

    mycourse = construct_course(courseId, courseName)

    assert mycourse.getId() == courseId
    assert mycourse.getName() == courseName


def test_setName():
    courseName = 'new name'
    courseId = 3

    mycourse = construct_course(courseId, 'initial name')
    mycourse = mycourse.setName(courseName)

    assert mycourse.getId() == courseId
    assert mycourse.getName() == courseName


def test_course_toString():
    courseName = 'my course'
    courseId = 3

    mycourse = construct_course(courseId, courseName)

    assert mycourse.toString() == str(courseId) + courseName


