from typing import NamedTuple
from collections.abc import Callable

from course import Course, construct_course
from data_source import DataSource

# https://stackoverflow.com/questions/50530959/generic-namedtuple-in-python-3-6


class Repository(NamedTuple):
    getAll: Callable[[], list[Course]]
    get: Callable[[int], Course]
    post: Callable[[Course], None]
    put: Callable[[Course], None]
    delete: Callable[[Course], None]

def construct_course_repository(data_source: DataSource) -> Repository:
    def getAll() -> list[Course]:
        return data_source.getCourseList()


    def get(id: int) -> Course:
        return [course for course in getAll() if course.getId() == id][0]


    def post(course: Course) -> None:
        getAll().append(course)


    def put(course: Course) -> None:
        tobeReplaced = get(course.getId())
        index = getAll().index(tobeReplaced)
        getAll()[index] = course


    def delete(course: Course) -> None:
        tobeRemoved = get(course.getId())
        getAll().remove(tobeRemoved)


    return Repository(getAll=getAll,
                    get=get,
                    post=post,
                    put=put,
                    delete=delete)
