from data_source import construct_data_source
from course import construct_course, Course
from feedback import construct_feedback, Feedback

def test_getCourseList():
    mydata = construct_data_source(
        [construct_course(1, 'Course One'), construct_course(2, 'Course Two')],
        [])

    assert len(mydata.getCourseList()) == 2


def test_getFeedbackList():
    mydata = construct_data_source(
        [],
        [construct_feedback('Feedback One', 2), construct_feedback('Feedback Two', 1), construct_feedback('Feedback Three', 5)])

    assert len(mydata.getFeedbackList('course name')) == 3
